@echo off

    if exist "bubbleSort.obj" del "bubbleSort.obj"
    if exist "bubbleSort.exe" del "bubbleSort.exe"

    \masm32\bin\ml /c /coff "bubbleSort.asm"
    if errorlevel 1 goto errasm

    \masm32\bin\PoLink /SUBSYSTEM:CONSOLE "bubbleSort.obj"
    if errorlevel 1 goto errlink
    dir "bubbleSort.*"
    goto TheEnd

  :errlink
    echo _
    echo Link error
    goto TheEnd

  :errasm
    echo _
    echo Assembly Error
    goto TheEnd
    
  :TheEnd

pause