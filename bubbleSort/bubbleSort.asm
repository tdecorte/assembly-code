
; Tracey DeCorte
; CST250 / Assign 2
; Assembly
; Bubble Sort
; July 30, 2012


include \masm32\include\masm32rt.inc

.data

    count dd 100
    counter dd 10
    esVal dd 10
    edVal dd 9
    zero dd 0
    
    sum dd 0
    idx2 dd 0

    ;aa DWORD 10 DUP(7, 5, 6, 1, 4, 3, 9, 2, 10, 8) 
    ;aa DWORD 10 DUP(8, 6, 3, 2, 5, 9, 4, 10, 6, 7) 
    aa DWORD 10 DUP(5, 7, 6, 1, 4, 3, 9, 2, 10, 8)

    

.code

start:

    cls
    print chr$("Sorted Array:", 13, 10)

    mov ECX, count                      ; set count to 100    


reset_loop:

    mov EDI, edVal                      ; set lower idx
    mov ESI, esVal                      ; set higher idx


loop_top:

    dec ECX                             ; decrement count
    jz print_loop                       ; if count == 0, print contents of array
    jmp sort_loop                       ; else go to sort loop


sort_loop:
    
    mov EAX, [aa + EDI * 4]             ; set lower idx to EAX register    
    mov EBX, [aa + ESI * 4]             ; set higher idx to EBX register

    cmp EAX, EBX                        
    jg swap                             ; if lower idx is greater, go to swap
    
    dec ESI                             ; decrement higher idx
    dec EDI                             ; decrement lower idx
    jz reset_loop
     
    jmp loop_top                        ; jump back to top



swap:

    inc ECX                             ; increment counter
    mov EAX, [aa + EDI * 4]             
    mov EBX, [aa + ESI * 4]

    mov [aa + EDI * 4], EBX              ; mover higher idx contents to lower idx register
    mov [aa + ESI * 4], EAX              ; move lower idx contents to higher idx register
    dec ESI                              ; increment higher idx
    dec EDI                              ; increment lower idx 
    jz reset_loop
    jmp loop_top                         ; jump back to top



print_loop:    
    
    sub ESI, ESI                        ; Clear ESI register
    mov ESI, 0                          ; Set to zero
    mov EDI, 10                         ; Initialize EDI to size of array
    jmp print_loop_top



print_loop_top:
    
    inc ESI                             ; Increment array index
    jmp finish_loop

    

finish_loop:

    mov EAX, [aa + ESI * 4]             ; Enter contents of array index into EAX
    mov sum, EAX                        ; Move to sum
    print chr$(" ")                     ; Print to screen
    print str$(sum)

    dec EDI                             ; Decrement count from array size
    jz done                             ; If zero, end
        
    jmp print_loop_top


   

done:

    print chr$(13, 10, 13, 10)
    exit
   

end start









