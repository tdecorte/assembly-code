Bubble sort algorithm implemented in assembly code (MASM). 

This program sorts an array, defined at the beginning of the program, in order from least to greatest.

Unsorted array: 5, 7, 6, 1, 4, 3, 9, 2, 10, 8
Output of program: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10